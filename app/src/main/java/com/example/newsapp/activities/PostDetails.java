package com.example.newsapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.newsapp.R;

public class PostDetails extends AppCompatActivity {

    private ImageView imageView;
    private TextView title,message,category,date;
    private Bundle mGetValue;
    private String newsTitle,newsMessage,newsCategory,newsDate,imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        init();
        getAndSetIntentValues();
    }

    private void getAndSetIntentValues() {
        mGetValue = getIntent().getExtras();
        newsTitle = mGetValue.getString("title");
        newsMessage = mGetValue.getString("message");
        newsCategory=mGetValue.getString("category");
        newsDate = mGetValue.getString("date");
        imagePath = mGetValue.getString("image_path");

        title.setText(newsTitle);
        date.setText(newsDate);
        message.setText(newsMessage);
        category.setText(newsCategory);

        //todo for loading image from path

        Glide.with(PostDetails.this).load(imagePath).into(imageView);
    }

    private void init() {
        imageView = findViewById(R.id.news_image);
        title =findViewById(R.id.news_title);
        message = findViewById(R.id.news_message);
        category = findViewById(R.id.news_category);
        date = findViewById(R.id.news_date);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(PostDetails.this,Home.class));
        finish();
    }
}
