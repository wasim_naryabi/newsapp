package com.example.newsapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.newsapp.R;
import com.example.newsapp.adapters.AdminNewsAdapter;
import com.example.newsapp.adapters.NewsAdapter;
import com.example.newsapp.models.News;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Home extends AppCompatActivity {

    RecyclerView mNewsList;
    ArrayList<News> mNews;
    NewsAdapter mNewsAdopter;
    LinearLayoutManager mLinearLayoutManager;
    ProgressDialog progressDialog;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        loadFireBaseDate();
    }

    private void loadFireBaseDate() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child("news");
        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()){
                    progressDialog.dismiss();
                    News news = dataSnapshot.getValue(News.class);
                    mNews.add(news);
                    mNewsAdopter = new NewsAdapter(mNews,Home.this);
                    mNewsAdopter.notifyDataSetChanged();
                    mNewsList.setAdapter(mNewsAdopter);

                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private void init() {

        mNewsList = findViewById(R.id.news_list);
        mNews = new ArrayList<News>();
        mLinearLayoutManager= new LinearLayoutManager(this);
        mNewsList.setLayoutManager(mLinearLayoutManager);
        progressDialog = new ProgressDialog(Home.this);
        progressDialog.setMessage("Data loading");
        progressDialog.show();

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(Home.this)
                .setTitle("Exit Application !")
                .setIcon(R.drawable.exit)
                .setMessage("Are you sure to Exit!")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }
}
