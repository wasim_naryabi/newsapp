package com.example.newsapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.newsapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class AddNews extends AppCompatActivity {

    private EditText mTitle,mMessage;
    private Spinner mCategory;
    private ImageView mImage;

    private String newsTitle,newsMessage,newsCategory,newsDate,imagePath;

    private StorageReference mStorageRef;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);
        init();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    private void init() {
        mTitle = findViewById(R.id.title);
        mMessage = findViewById(R.id.message);
        mTitle = findViewById(R.id.title);
        mCategory = findViewById(R.id.categories_list);
        mImage = findViewById(R.id.image);


        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mCategory.setAdapter(dataAdapter);

    }

    public void selectImage(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,0);
    }

    public void addNews(View view) {
        newsTitle = mTitle.getText().toString();
        newsMessage = mMessage.getText().toString();
        newsCategory = mCategory.getSelectedItem().toString();
        newsDate =new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

        if(!imagePath.equals("")){
            HashMap<String,String> myData = new HashMap<String,String>();
            myData.put("category",newsCategory);
            myData.put("date",newsDate);
            myData.put("path",imagePath);
            myData.put("title",newsTitle);
            myData.put("message",newsMessage);

            databaseReference.child("news").child(newsTitle).setValue(myData).addOnCompleteListener(AddNews.this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Toast.makeText(AddNews.this, "News Added", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddNews.this,AdminHome.class));
                        finish();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Select jpg file", Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    mImage.setImageURI(selectedImage);

                    mStorageRef.child("images/").child(System.currentTimeMillis()+".jpg").putFile(selectedImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            if (taskSnapshot.getMetadata() != null) {
                                if (taskSnapshot.getMetadata().getReference() != null) {
                                    Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                                    result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            imagePath = uri.toString();
                                            Toast.makeText(AddNews.this, "Path selected", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }else{
                                    imagePath="no_image";
                                    Toast.makeText(AddNews.this, "Path not selected", Toast.LENGTH_SHORT).show();
                                }
                            }
                            Toast.makeText(AddNews.this, "image uploaded", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AddNews.this,AdminHome.class));
        finish();
    }
}
