package com.example.newsapp.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.newsapp.R;

public class AdminLogin extends Activity {

    private EditText emailField;
    private EditText passwordField;
    private Button loginBtn;
    private TextView signUpBtn;
    private String userEmail,
            userPassword;

    ProgressBar regProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
        init();
        regProgress.setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(AdminLogin.this)
                .setTitle("Exit Application !")
                .setIcon(R.drawable.exit)
                .setMessage("Are you sure to Exit!")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }

    private void init() {
        emailField = findViewById(R.id.emailLoginField);
        passwordField = findViewById(R.id.passwordLoginField);
        loginBtn =findViewById(R.id.loginButton);
        signUpBtn =findViewById(R.id.newUserBtn);
        regProgress =findViewById(R.id.progressBar);
    }

    public void userLogin(View view) {
        startActivity(new Intent(AdminLogin.this,UserLogin.class));
        finish();
    }

    public void adminLogin(View view) {
        loginUser();
    }

    private void loginUser() {
        userEmail = emailField.getText().toString().trim();
        userPassword = passwordField.getText().toString().trim();


        if(userEmail.isEmpty()){
            emailField.setError("Email Required");
        }else if(userPassword.isEmpty()){
            passwordField.setError("password Required");
        }else if (userPassword.length() < 6) {
            passwordField.setError("At least 6 character password");
        }else if ((!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())) {
            emailField.setError("Wrong format");
        }else if(userEmail.equals("admin@gmail.com") && userPassword.equals("admin1234")){
            startActivity(new Intent(AdminLogin.this,AdminHome.class));
            finish();
        }
    }
}
