package com.example.newsapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class UserSignup extends Activity {

    private EditText nameField,
            emailField,
            passwordField,
            conPasswordField;
    private Button signupBtn;
    private TextView loginScreeBtn;
    private String userName,
            userEmail,
            userPassword,
            userConPassword;

    boolean netConnection;

    ProgressBar regProgress;

    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_signup);
        init();
        checkConnection();
        regProgress.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newUserSignUp();
            }
        });

        loginScreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginScreen = new Intent(UserSignup.this,UserLogin.class);
                startActivity(loginScreen);
                finish();
            }
        });
    }

    private void newUserSignUp() {

        userName=nameField.getText().toString().trim();
        userEmail=emailField.getText().toString().trim();
        userPassword=passwordField.getText().toString().trim();
        userConPassword=conPasswordField.getText().toString().trim();

        if(netConnection) {


            if (userName.equals("")) {
                nameField.setError("Name required!");
            } else if (userEmail.equals("")) {
                emailField.setError("Email required!");
            } else if (userPassword.equals("")) {
                passwordField.setError("Password Required!");
            } else if (userPassword.length() < 6) {
                passwordField.setError("At least 6 character password");
            } else if ((!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())) {
                emailField.setError("Wrong format");
            } else if (!(userPassword.equals(userConPassword))) {
                conPasswordField.setError("password not match");
            } else {
                // todo new user registration
                mAuth.createUserWithEmailAndPassword(userEmail,userPassword).addOnCompleteListener(UserSignup.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String currentUser = mAuth.getCurrentUser().getUid();

                            HashMap<String,String> myData = new HashMap<String,String>();
                            myData.put("name",userName);
                            myData.put("id",currentUser);
                            myData.put("email",userEmail);

                            databaseReference.child("users").child(currentUser).setValue(myData).addOnCompleteListener(UserSignup.this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(UserSignup.this, "User saved", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(UserSignup.this, "Error", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });




                            startActivity(new Intent(UserSignup.this,Home.class));
                            Toast.makeText(UserSignup.this, "Welcome"+ currentUser, Toast.LENGTH_SHORT).show();
                            finish();

                        }else{
                            String error = task.getException().toString();
                            Toast.makeText(UserSignup.this, error, Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        }
        else{

        }

    }

    private void init() {
        nameField = findViewById(R.id.nameSignUpField);
        emailField = findViewById(R.id.emailSignUpField);
        passwordField = findViewById(R.id.passwordSignUpField);
        conPasswordField = findViewById(R.id.conPasswordSignUpField);
        signupBtn = findViewById(R.id.signUpBtn);
        loginScreeBtn =findViewById(R.id.haveAccountBtn);
        regProgress = findViewById(R.id.progressBar);
    }

    public void checkConnection(){
        netConnection=false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            netConnection = true;
        }
        else{
            netConnection = false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserSignup.this,UserLogin.class));
        finish();
    }
}
