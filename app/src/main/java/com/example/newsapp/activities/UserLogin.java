package com.example.newsapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class UserLogin extends Activity {
    private EditText emailField;
    private EditText passwordField;
    private Button loginBtn;
    private TextView signUpBtn;
    private String userEmail,
            userPassword;

    ProgressBar regProgress;
    boolean netConnection;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);
        init();
        checkConnection();
        regProgress.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLogin.this,UserSignup.class);
                startActivity(intent);
                finish();
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });
    }

    private void loginUser() {
        userEmail = emailField.getText().toString().trim();
        userPassword = passwordField.getText().toString().trim();


        if(userEmail.isEmpty()){
            emailField.setError("Email Required");
        }else if(userPassword.isEmpty()){
            passwordField.setError("password Required");
        }else if (userPassword.length() < 6) {
            passwordField.setError("At least 6 character password");
        }else if ((!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches())) {
            emailField.setError("Wrong format");
        }else if(netConnection){
            // todo user login

            mAuth.signInWithEmailAndPassword(userEmail,userPassword).addOnCompleteListener(UserLogin.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        startActivity(new Intent(UserLogin.this,Home.class));
                        finish();

                    }else{
                        String error = task.getException().toString();
                        Toast.makeText(UserLogin.this, error, Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(UserLogin.this)
                .setTitle("Exit Application !")
                .setIcon(R.drawable.exit)
                .setMessage("Are you sure to Exit!")
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }

    private void init() {
        emailField = findViewById(R.id.emailLoginField);
        passwordField = findViewById(R.id.passwordLoginField);
        loginBtn =findViewById(R.id.loginButton);
        signUpBtn =findViewById(R.id.newUserBtn);
        regProgress =findViewById(R.id.progressBar);
    }

    public void checkConnection(){
        netConnection=false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            netConnection = true;
        }
        else{
            netConnection = false;
        }
    }

    public void gotoAdminLogin(View view) {
        Intent loginScreen = new Intent(UserLogin.this,AdminLogin.class);
        startActivity(loginScreen);
        finish();
    }
}
