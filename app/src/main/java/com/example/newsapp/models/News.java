package com.example.newsapp.models;

public class News {

    private String title,message,path,category,date;

    public News() {
    }

    public News(String title, String message, String path, String category, String date) {
        this.title = title;
        this.message = message;
        this.path = path;
        this.category = category;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public String getCategory() {
        return category;
    }

    public String getDate() {
        return date;
    }
}
