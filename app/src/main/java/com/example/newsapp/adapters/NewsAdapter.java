package com.example.newsapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.newsapp.R;
import com.example.newsapp.activities.PostDetails;
import com.example.newsapp.models.News;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    ArrayList<News> news = new ArrayList<News>();
    Context context;

    public NewsAdapter(ArrayList<News> news, Context context) {
        this.news = news;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder myView = new ViewHolder(LayoutInflater.from(context).inflate(R.layout.news_item,parent,false));
        return myView;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final News newsItem = news.get(position);
        holder.title.setText(newsItem.getTitle());
        holder.message.setText(newsItem.getMessage());
        holder.category.setText(newsItem.getCategory());
        holder.date.setText(newsItem.getDate());
        Glide.with(context).load(newsItem.getPath()).into(holder.imageView);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PostDetails.class);
                intent.putExtra("title",newsItem.getTitle());
                intent.putExtra("message",newsItem.getMessage());
                intent.putExtra("category",newsItem.getCategory());
                intent.putExtra("date",newsItem.getDate());
                intent.putExtra("image_path",newsItem.getPath());
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView title,message,category,date;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.news_image);
            title = itemView.findViewById(R.id.news_title);
            message = itemView.findViewById(R.id.news_message);
            category = itemView.findViewById(R.id.news_category);
            date = itemView.findViewById(R.id.news_date);
        }
    }
}
